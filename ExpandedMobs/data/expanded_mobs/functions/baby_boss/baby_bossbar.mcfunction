execute at @e[type=minecraft:zombie,tag=baby_boss] run bossbar set zombie players @a[distance=..32]
execute store result bossbar zombie max run attribute @e[type=minecraft:zombie,tag=baby_boss,limit=1] generic.max_health get
execute store result bossbar zombie value run data get entity @e[type=minecraft:zombie,tag=baby_boss,limit=1] Health
execute if entity @e[type=minecraft:zombie,tag=baby_boss] run bossbar set zombie color green
execute if entity @e[type=minecraft:zombie,tag=baby_boss] run bossbar add zombie {"text":"Baby Boss", "color":"green"}
execute unless entity @e[type=minecraft:zombie,tag=baby_boss] run bossbar remove zombie