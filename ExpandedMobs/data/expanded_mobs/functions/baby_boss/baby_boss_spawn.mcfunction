###Trigger on zombies when the time is right###

execute if score baby_boss_count boss_spawn matches ..0 run function expanded_mobs:baby_boss/baby_boss_spawn_function

### If the mob is not a baby boss ###
tag @s add not_baby_boss
### Reset
execute if score baby_boss_count boss_spawn matches ..0 run scoreboard players set baby_boss_count boss_spawn 100000