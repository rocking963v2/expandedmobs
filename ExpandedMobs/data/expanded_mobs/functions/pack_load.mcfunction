tellraw @p {"text":"Welcome to Expanded Mobs (and more)","color":"blue"}

## INIT Scoreboards ##
scoreboard objectives add boss_spawn dummy

##Soul Stuff##
scoreboard objectives add soul_storage_max dummy
scoreboard objectives add soul_storage dummy
scoreboard objectives add player_damage minecraft.custom:minecraft.damage_dealt

##Soul Storage Level up##
scoreboard objectives add suspicious_stew_eat minecraft.used:minecraft.suspicious_stew
scoreboard objectives add soul_storage_upgrade_delay dummy

### Carrot on a stick detection###
scoreboard objectives add right_click_used minecraft.used:minecraft.carrot_on_a_stick

### Cooldown Timer###
scoreboard objectives add ticks dummy
scoreboard objectives add seconds dummy

###New Mob Attacks###
scoreboard objectives add attack_timer dummy

##Setting the score##
### Baby Boss at 100,000 ticks ###
scoreboard players set baby_boss_count boss_spawn 100000

###Warden at 250,000 ticks###
scoreboard players set warden_boss_count boss_spawn 250000

###Scorched at 175,000 ticks###
scoreboard players set scorched_boss_count boss_spawn 175000
