execute at @e[type=minecraft:wither_skeleton,tag=the_scorched] run bossbar set wither_skeleton players @a[distance=..32]
execute store result bossbar wither_skeleton max run attribute @e[type=minecraft:wither_skeleton,tag=the_scorched,limit=1] generic.max_health get
execute store result bossbar wither_skeleton value run data get entity @e[type=minecraft:wither_skeleton,tag=the_scorched,limit=1] Health
execute if entity @e[type=minecraft:wither_skeleton,tag=the_scorched] run bossbar set wither_skeleton color red
execute if entity @e[type=minecraft:wither_skeleton,tag=the_scorched] run bossbar add wither_skeleton {"text":"The Scorched", "color":"red"}
execute unless entity @e[type=minecraft:wither_skeleton,tag=the_scorched] run bossbar remove wither_skeleton