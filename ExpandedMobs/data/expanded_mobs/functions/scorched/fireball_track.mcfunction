###Explodes on person
execute if entity @e[type=player,distance=0..2] run function expanded_mobs:scorched/fireball_hit_player

###Explodes on block
execute unless block ~ ~ ~ air run function expanded_mobs:scorched/fireball_hit_wall

#Moves Forward
execute at @s run tp @s ^ ^ ^0.3
particle minecraft:flame ~ ~ ~ 0 0 0 0.1 1
