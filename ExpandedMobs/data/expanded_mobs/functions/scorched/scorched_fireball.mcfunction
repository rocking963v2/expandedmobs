execute as @s[scores={attack_timer=0}] run playsound minecraft:entity.blaze.shoot hostile @a[distance=0..32] ~ ~ ~ 1 1 1
###Attack ###
execute as @s[scores={attack_timer=1}] run function expanded_mobs:scorched/fireballs

execute as @e[tag=scorched_fireball1,tag=not_rotated] facing entity @p feet run tp @s ~ ~1.5 ~ ~20 ~
execute as @e[tag=scorched_fireball2,tag=not_rotated] facing entity @p feet run tp @s ~ ~1.5 ~ ~ ~
execute as @e[tag=scorched_fireball3,tag=not_rotated] facing entity @p feet run tp @s ~ ~1.5 ~ ~-20 ~
execute as @e[tag=not_rotated] run tag @s remove not_rotated

execute as @s[scores={attack_timer=0}] run scoreboard players set @s attack_timer 200