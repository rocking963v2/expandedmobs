###Trigger on skeletons when the time is right###

execute if score scorched_boss_count boss_spawn matches ..0 run function expanded_mobs:scorched/scorched_spawn_function
scoreboard players add @e[type=wither_skeleton,tag=the_scorched] attack_timer 200

### If the mob is not a baby boss ###
tag @s add not_scorched
### Reset
execute if score scorched_boss_count boss_spawn matches ..0 run scoreboard players set scorched_boss_count boss_spawn 175000