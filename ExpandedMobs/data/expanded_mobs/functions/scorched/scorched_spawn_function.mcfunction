summon minecraft:wither_skeleton ~ ~1 ~ {CustomName:"\"The Scorched\"",DeathLootTable:"expanded_mobs:the_scorched",Health:75f,Attributes:[{Name:"minecraft:generic.max_health",Base:75},{Name:"minecraft:generic.movement_speed",Base:0.25}],Tags:["the_scorched"],ActiveEffects:[{Id:24b,Amplifier:1b,Duration:9999999,ShowParticles:0b}],HandItems:[{Count:1,id:bow,tag:{Enchantments:[{id:punch,lvl:2}]}}],HandDropChances:[0f],ArmorItems:[{Count:1,id:netherite_boots},{Count:1,id:netherite_leggings},{Count:1,id:netherite_chestplate},{Count:1,id:netherite_helmet}],ArmorDropChances:[0f,0f,0f,0f]}
tp @s ~ ~-500 ~
tellraw @p {"text":"The Scorched has summoned near ","extra":[{"selector":"@p"},{"text":"!"}],"color":"yellow"}
playsound expanded_sounds:boss_one hostile @a[distance=0..64] ~ ~ ~ 100 1 1
