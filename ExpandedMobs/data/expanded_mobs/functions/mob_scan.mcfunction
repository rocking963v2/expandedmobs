###Scanning for baby boss spawn###
execute as @e[type=zombie,tag=!not_baby_boss,tag=!baby_boss,sort=random] at @s run function expanded_mobs:baby_boss/baby_boss_spawn
###Scanning for warden boss spawn###
execute as @e[type=enderman,tag=!not_warden,tag=!the_warden,sort=random] at @s run function expanded_mobs:warden/warden_spawn
###Scanning for Scorched boss spawn###
execute as @e[type=skeleton,tag=!not_scorched,tag=!the_scorched,sort=random] at @s run function expanded_mobs:scorched/scorched_spawn

### Reduces spawn cooldown if no baby boss is present
execute unless entity @e[type=zombie,tag=baby_boss] run scoreboard players remove baby_boss_count boss_spawn 1
### Reduces spawn cooldown if no warden boss is present
execute unless entity @e[type=warden,tag=the_warden] run scoreboard players remove warden_boss_count boss_spawn 1
### Reduces spawn cooldown if no scorched boss is present
execute unless entity @e[type=wither_skeleton,tag=the_scorched] run scoreboard players remove scorched_boss_count boss_spawn 1