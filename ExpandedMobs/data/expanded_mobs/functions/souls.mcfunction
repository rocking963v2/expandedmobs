###Soul Gather###
execute as @a[scores={player_damage=20..},nbt={SelectedItem:{tag:{"soul_gather":1b}}}] if score @s soul_storage < @s soul_storage_max at @s run function expanded_mobs:soul_gather
scoreboard players set @p player_damage 0

###Heal###
execute as @a[scores={right_click_used=1..,seconds=0},nbt={SelectedItem:{tag:{"soul_heal":1b}}}] if score @s soul_storage matches 10.. at @s run function expanded_mobs:soul_heal
scoreboard players set @p right_click_used 0

execute if entity @a[scores={soul_storage_max=1..},nbt={SelectedItem:{tag:{"soul_see":1b}}}] run title @p actionbar [{"text":"Souls <"},{"score":{"objective":"soul_storage","name":"@p"}},{"text":"/"},{"score":{"objective":"soul_storage_max","name":"@p"}},{"text":"> "},{"text":"Cooldown: <","color": "red"},{"score":{"objective":"seconds","name":"@p"},"color": "red"},{"text":">","color": "red"}]
