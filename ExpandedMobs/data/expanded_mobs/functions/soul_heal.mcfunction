scoreboard players remove @s soul_storage 10
scoreboard players set @s seconds 10
scoreboard players set @s ticks 20
effect give @s minecraft:instant_health 1 1
particle minecraft:heart ~ ~2 ~ 0.1 0.1 0.1 0.1 3
playsound minecraft:entity.evoker.prepare_summon player @s ~ ~2 ~ 60 1 1