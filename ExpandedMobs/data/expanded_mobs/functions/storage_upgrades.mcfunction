scoreboard players set @a[nbt={SelectedItem:{id:"minecraft:suspicious_stew",tag:{basic_soul_upgrade:1b}}}] soul_storage_upgrade_delay 2
execute as @a[scores={soul_storage_upgrade_delay=1..,suspicious_stew_eat=1}] at @s run function expanded_mobs:soul_upgrade_test

scoreboard players reset @a[scores={suspicious_stew_eat=1..}] suspicious_stew_eat
execute as @a[scores={soul_storage_upgrade_delay=1..}] unless entity @s[nbt={SelectedItem:{id:"minecraft:suspicious_stew",tag:{basic_soul_upgrade:1b}}}] run scoreboard players remove @s soul_storage_upgrade_delay 1