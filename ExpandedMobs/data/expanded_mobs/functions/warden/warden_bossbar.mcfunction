execute at @e[type=minecraft:warden,tag=the_warden] run bossbar set warden players @a[distance=..32]
execute store result bossbar warden max run attribute @e[type=minecraft:warden,tag=the_warden,limit=1] generic.max_health get
execute store result bossbar warden value run data get entity @e[type=minecraft:warden,tag=the_warden,limit=1] Health
execute if entity @e[type=minecraft:warden,tag=the_warden] run bossbar set warden color purple
execute if entity @e[type=minecraft:warden,tag=the_warden] run bossbar add warden {"text":"The Warden", "color":"dark_purple"}
execute unless entity @e[type=minecraft:warden,tag=the_warden] run bossbar remove warden